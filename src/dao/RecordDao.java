/**
 * @File:        RecordDao.java
 * @Package:     dao
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午3:22:50
 */

package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.Record;
import util.DBUtil;

/**
 * @ClassName:   RecordDao
 * @Description: 记一笔DAO
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午3:22:50
 */
public class RecordDao {
//	初始化驱动程序
	public RecordDao() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

//	获取数据库链接
	public Connection getConnection() {
		return DBUtil.getConnection();
	}

//	CRUD
//	增加
	public void add(Record record) {
		String sql = "insert into record values(null,?,?,?,?)";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1,record.getSpend());
			ps.setInt(2, record.getcid());
			ps.setString(3, record.getComment());
			ps.setDate(4, record.getDate());

			ps.execute();

			ResultSet rs=ps.getGeneratedKeys();
			if(rs.next()) {
				int id=rs.getInt(1);
				record.setId(id);
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	删除
	public void delete(int id) {
		try(Connection c=getConnection();Statement s=c.createStatement()){
			String sql="delete from record where id = " + id;
			s.execute(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	改正
	public void update(Record record) {
		String sql = "update record set spend=?,cid=?,comment=?,date=? where id=?";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1, record.getSpend());
			ps.setInt(2, record.getcid());
			ps.setString(3, record.getComment());
			ps.setDate(4,record.getDate());

			ps.execute();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	查询
	public Record get(int id) {
		Record record = null;
		try(Connection c = getConnection();Statement s = c.createStatement();){
			String sql = "select * from record where id = " +id;
			ResultSet rs = s.executeQuery(sql);
			if(rs.next()) {
				record = new Record();
				int spend = rs.getInt(2);
				int cid = rs.getInt("cid");
				String comment = rs.getString("comment");
				Date date = rs.getDate("date");
				record.setSpend(spend);
				record.setcid(cid);
				record.setComment(comment);
				record.setDate(date);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}

		return record;
	}

//	获取数量
	public int getTotal() {
		int total = 0;
		try(Connection c = getConnection();Statement s = c.createStatement();){
			String sql = "select count(*) from record";
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				total = rs.getInt(1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return total;
	}

//	获取全部对象
	public List<Record> list(){
		return list(0,Short.MAX_VALUE);
	}

//	获取部分对象
	public List<Record> list(int start,int count){
		List<Record> records = new ArrayList<Record>();
		String sql = "select * from record order by id desc limit ?,?";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1,start);
			ps.setInt(2,count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Record record = new Record();
				int id = rs.getInt("id");
				int spend = rs.getInt(2);
				int cid = rs.getInt("cid");
				String comment = rs.getString("comment");
				Date date = rs.getDate("date");
				record.setSpend(spend);
				record.setcid(cid);
				record.setComment(comment);
				record.setDate(date);
				records.add(record);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return records;
	}
}
