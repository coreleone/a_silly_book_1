/**
 * @File:        CategoryDao.java
 * @Package:     dao
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午3:14:52
 */

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.Category;
import util.DBUtil;

/**
 * @ClassName:   CategoryDao
 * @Description: 消费分类DAO
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午3:14:52
 */
public class CategoryDao {
//	初始化驱动程序
	public CategoryDao() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

//	获取数据库链接
	public Connection getConnection(){
		return DBUtil.getConnection();
	}

//	CRUD
//	增加
	public void add(Category category) {
		String sql = "insert into category values(null,?,?)";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setString(1,category.getName());
			ps.setString(2, category.getNumber());

			ps.execute();

			ResultSet rs=ps.getGeneratedKeys();
			if(rs.next()) {
				int id=rs.getInt(1);
				category.setId(id);
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	删除
	public void delete(int id) {
		try(Connection c=getConnection();Statement s=c.createStatement()){
			String sql="delete from category where id = " + id;
			s.execute(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	改正
	public void update(Category category) {
		String sql = "update category set name=?,number=? where id=?";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setString(1, category.getName());
			ps.setString(2, category.getNumber());
			ps.setInt(3,category.getId());

			ps.execute();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	通过id查询获取
	public Category get(int id) {
		Category category = null;
		try(Connection c = getConnection();Statement s = c.createStatement();){
			String sql = "select * from category where id = " +id;
			ResultSet rs = s.executeQuery(sql);
			if(rs.next()) {
				category = new Category();
				String name = rs.getString("name");
				String number = rs.getString("number");
				category.setId(id);
				category.setName(name);
				category.setNumber(number);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}

		return category;
	}

//	通过name查询获取
	public Category get(String name) {
		Category category = null;
		try(Connection c = getConnection();Statement s = c.createStatement();){
			String sql = "select * from category where name = " +"'"+name+"'";
			ResultSet rs = s.executeQuery(sql);
			if(rs.next()) {
				category = new Category();
				int id = rs.getInt("id");
				String number = rs.getString("number");
				category.setId(id);
				category.setName(name);
				category.setNumber(number);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}

		return category;
	}

//	获取数量
	public int getTotal() {
		int total = 0;
		try(Connection c = getConnection();Statement s = c.createStatement();){
			String sql = "select count(*) from category";
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				total = rs.getInt(1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return total;
	}

//	获取数据库中的name集合
	public ArrayList<String> getNameList(){
		List<Category> categorys = this.list();
		ArrayList<String> name = new ArrayList<String>();
		for(Category category : categorys) {
			name.add(category.getName());
		}

		return name;
	}

//	获取全部对象
	public List<Category> list(){
		return list(0,Short.MAX_VALUE);
	}

//	获取部分对象
	public List<Category> list(int start,int count){
		List<Category> categorys = new ArrayList<Category>();
		String sql = "select * from category order by id desc limit ?,?";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1,start);
			ps.setInt(2,count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Category category = new Category();
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String number = rs.getString("number");
				category.setId(id);
				category.setName(name);
				category.setNumber(number);
				categorys.add(category);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return categorys;
	}

}
