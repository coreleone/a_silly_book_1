/**
 * @File:        ConfigDao.java
 * @Package:     dao
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午1:10:33
 */

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.Config;
import util.DBUtil;

/**
 * @ClassName:   ConfigDao
 * @Description: 设置DAO
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午1:10:33
 */
public class ConfigDao {
//	初始化驱动程序
	public ConfigDao() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

//	获取数据库链接
	public Connection getConnection() {
		return DBUtil.getConnection();
	}

//	CRUD
//	增加
	public void add(Config config) {
		String sql = "insert into config values(null,?,?)";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setString(1,config.getKey());
			ps.setString(2, config.getValue());

			ps.execute();

			ResultSet rs=ps.getGeneratedKeys();
			if(rs.next()) {
				int id=rs.getInt(1);
				config.setId(id);
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	删除
	public void delete(int id) {
		try(Connection c=getConnection();Statement s=c.createStatement()){
			String sql="delete from config where id = " + id;
			s.execute(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	改正
	public void update(Config config) {
		String sql = "update config set key_=?,value=? where id=?";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setString(1, config.getKey());
			ps.setString(2, config.getValue());
			ps.setInt(3,config.getId());

			ps.execute();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

//	查询
	public Config get(int id) {
		Config config = null;
		try(Connection c = getConnection();Statement s = c.createStatement();){
			String sql = "select * from config where id = " +id;
			ResultSet rs = s.executeQuery(sql);
			if(rs.next()) {
				config = new Config();
				String key = rs.getString(2);
				String value = rs.getString("value");
				config.setKey(key);
				config.setValue(value);
				config.setId(id);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}

		return config;
	}

//	获取数量
	public int getTotal() {
		int total = 0;
		try(Connection c = getConnection();Statement s = c.createStatement();){
			String sql = "select count(*) from config";
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				total = rs.getInt(1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return total;
	}

//	获取最新一条数据
	public Config getTheLastOne() {
		Config config = null;
		try(Connection c = getConnection();Statement s = c.createStatement();){
			String sql = "select * from config where id = (select max(id) from config)";
			ResultSet rs = s.executeQuery(sql);
			if(rs.next()) {
				config = new Config();
				config.setId(rs.getInt("id"));
				config.setKey(rs.getString("key_"));
				config.setValue(rs.getString("value"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return config;
	}

//	获取全部对象
	public List<Config> list(){
		return list(0,Short.MAX_VALUE);
	}

//	获取部分对象
	public List<Config> list(int start,int count){
		List<Config> configs = new ArrayList<Config>();
		String sql = "select * from config order by id desc limit ?,?";
		try(Connection c = getConnection();PreparedStatement ps = c.prepareStatement(sql);){
			ps.setInt(1,start);
			ps.setInt(2,count);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Config config = new Config();
				int id = rs.getInt("id");
				String key = rs.getString("key_");
				String value = rs.getString("value");
				config.setId(id);
				config.setValue(value);
				config.setKey(key);
				configs.add(config);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return configs;
	}
}
