/**
 * @File:        Bootstrap.java
 * @Package:     startup
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午3:41:56
 */

package startup;

import gui.frame.MainFrame;

/**
 * @ClassName:   Bootstrap
 * @Description: 启动类
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午3:41:56
 */
public class Bootstrap {
	/**
	 * @Title:       main
	 * @Description: 主方法
	 * @Author: 	 橙子
	 * @param:       @param args
	 * @return:      void
	 * @throws:
	 */
	public static void main(String[] args) {
////		设置皮肤
//		try {
//			JFrame.setDefaultLookAndFeelDecorated(true);
//			JDialog.setDefaultLookAndFeelDecorated(true);
//			SwingUtilities.invokeAndWait(new Runnable() {
//				public void run() {
//					SubstanceLookAndFeel.setSkin(new BusinessBlackSteelSkin());
//				}
//			});
//
//		}catch(Exception e) {
//			e.printStackTrace();
//		}

//		获取主框架并显示
		MainFrame.getInstance().mainFrame.setVisible(true);

////		新建一个线程更新月消费报表数据
//		new Thread(new ChartUtil().new reportDataUpdate()).start();
////		新建一个线程更新消费一览数据
//		new Thread(new SpendService()).start();
	}
}
