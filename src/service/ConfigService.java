/**
 * @File:        ConfigService.java
 * @Package:     service
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午4:37:24
 */

package service;

import javax.swing.JOptionPane;

import dao.ConfigDao;
import entity.Config;
import gui.panel.ConfigPanel;

/**
 * @ClassName:   ConfigService
 * @Description: 设置数据服务
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午4:37:24
 */
public class ConfigService {
//	饿汉式单例模式
	private ConfigService() {

	}
	private static ConfigService instance = new ConfigService();
	public static ConfigService getInstance() {
		if(null == instance) {
			instance = new ConfigService();
		}

		return instance;
	}

//	更新
	public void updateService(String budget,String mysqlContent) {
		if(!budget.matches("[0-9]+")) {
//			如果预算数值不符合零或者正整数
			JOptionPane.showMessageDialog(ConfigPanel.getInstance().configPanel,"请输入零或者正整数");
		}else {
//			如果预算数值符合零或者正整数
			Config config = new Config();//设置Entity类
			config.setKey(budget);//设置key
			config.setValue(mysqlContent);//设置value

			ConfigDao configDao = new ConfigDao();//设置DAO类
			configDao.add(config);//添加到数据库
		}
	}
}
