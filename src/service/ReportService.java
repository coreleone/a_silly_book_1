/**
 * @File:        ReportService.java
 * @Package:     service
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月3日 下午7:43:13
 */

package service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.jfree.data.time.Day;
import org.jfree.data.time.TimeTableXYDataset;

import dao.RecordDao;
import entity.Record;

/**
 * @ClassName:   ReportService
 * @Description: 月消费报告数据服务
 * @Author:    	 橙子
 * @Date:        2021年1月3日 下午7:43:13
 */
public class ReportService {
//	饿汉式单例模式
	private ReportService() {

	}
	private static ReportService instance = new ReportService();
	public static ReportService getInstance() {
		if(null == instance) {
			instance = new ReportService();
		}

		return instance;
	}

//	获取图表数据集
	public TimeTableXYDataset getDataSet() {
		TimeTableXYDataset data = new TimeTableXYDataset();//数据集

//		设置数据
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);//本月
		int daySpend;//日消费

		for(int i = 1;i<Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);i++) {
//			从一日开始，逐日增加，直至本月最大日
			daySpend = 0;//将日消费赋值为0
			ArrayList<Record> records = (ArrayList<Record>)new RecordDao().list();//记一笔数据集合
			Iterator<Record> it = records.iterator();
			if(!records.isEmpty()) {
			//如果记一笔数据集合不为空
				while(it.hasNext()) {
					Record record = it.next();
					if(record.getDate() != null) {
//						逐个查找记一笔数据
						c.setTime(new java.util.Date(record.getDate().getTime()));//记一笔数据的日期
						if(month == c.get(Calendar.MONTH) && i == c.get(Calendar.DAY_OF_MONTH)){
							//与本月相同且与i日相同
							daySpend += record.getSpend();//加值赋值
						}
					}
				}
				Date currentTime = new Date(System.currentTimeMillis());
				c.setTime(currentTime);
				c.set(Calendar.DAY_OF_MONTH,i);
				data.add(new Day(c.getTime()), daySpend, "消费金额");//设置数据集
			}else {
				return null;//返回空
			}
		}

		return data;//返回数据集

	}
}
