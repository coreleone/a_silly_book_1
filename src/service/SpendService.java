/**
 * @File:        SpendService.java
 * @Package:     service
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月2日 上午2:02:48
 */

package service;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import dao.ConfigDao;
import dao.RecordDao;
import entity.Config;
import entity.Record;

/**
 * @ClassName:   SpendService
 * @Description: 消费一览数据报告
 * @Author:    	 橙子
 * @Date:        2021年1月2日 上午2:02:48
 */
public class SpendService implements Runnable{
//	获取数据库中Record数据
	public List<Record> recordList = new RecordDao().list();
	public Iterator<Record> it = recordList.iterator();

//	本月消费与今日消费
	public int monthSpend = 0;
	public int daySpend = 0;
//	日均消费、本月剩余、日均可用与距离月末
	public int avgSpendPerDay = 0;
	public int restofThisMonth = 0;
	public int avgDailyAvailable = 0;
	public int monthLeftDay = 0;

//	饿汉式单例模式
	private SpendService() {

	}
	private static SpendService instance = new SpendService();
	public static SpendService getInstance() {
		if(null == instance) {
			instance = new SpendService();
		}

		return instance;
	}

//	run方法
	public void run() {
//		this.addListener();
	}

	public void getSpendPanelData() {
//		获取日历
		Calendar cal = Calendar.getInstance();
		Date currentTime = new Date(System.currentTimeMillis());
//		本年
		int currentYear = cal.get(Calendar.YEAR);
//		本月
		int currentMonth = cal.get(Calendar.MONTH);
//		本日
		int currentDay = cal.get(Calendar.DAY_OF_MONTH);
//		本月多少日
		int allDayOfThisMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		while(it.hasNext()) {
			Record record = it.next();
//			数据库中的日期
			Date recordDate = record.getDate();
//			更改日期
			cal.setTime(recordDate);
//			获取年份
			int getYear = cal.get(Calendar.YEAR);
//			获取月份
			int getMonth = cal.get(Calendar.MONTH);
//			获取日期
			int getDay = cal.get(Calendar.DAY_OF_MONTH);

//			本月消费
			if(getMonth == currentMonth && getYear == currentYear) {
				monthSpend += record.getSpend();
			}
//			今日消费
			if(getDay == currentDay && getMonth == currentMonth && getYear == currentYear) {
				daySpend += record.getSpend();
			}
		}

//		日均消费、本月剩余、日均可用与距离月末
//		更改日期
		cal.setTime(currentTime);
//		日均消费
		avgSpendPerDay = monthSpend/cal.get(Calendar.DAY_OF_MONTH);
//		本月剩余与日均可用
		Config config = new ConfigDao().getTheLastOne();
		if(config != null) {
			int budgetCount = Integer.parseInt(config.getKey());
			SpendService.getInstance().restofThisMonth = budgetCount-SpendService.getInstance().monthSpend;
			SpendService.getInstance().avgDailyAvailable = SpendService.getInstance().restofThisMonth/(allDayOfThisMonth-currentDay);
		}else {
			SpendService.getInstance().restofThisMonth = -SpendService.getInstance().monthSpend;
			SpendService.getInstance().avgDailyAvailable = 0;
		}

//		具体月末
		monthLeftDay = allDayOfThisMonth-currentDay;

	}
}