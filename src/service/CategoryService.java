/**
 * @File:        CategoryService.java
 * @Package:     service
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午5:35:12
 */

package service;

import dao.CategoryDao;
import entity.Category;

/**
 * @ClassName:   CategoryService
 * @Description: 消费分类数据服务
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午5:35:12
 */
public class CategoryService {
//	饿汉式单例模式
	private CategoryService() {

	}
	private static CategoryService instance = new CategoryService();
	public static CategoryService getInstance() {
		if(null == instance) {
			instance = new CategoryService();
		}

		return instance;
	}

//	消费分类的Entity类和DAO类
	private Category category = new Category();
	private CategoryDao categoryDao = new CategoryDao();

//	临时id
	private int tempId;

//	新增
	public void addService(String add,String number) {
		category.setName(add);//设置name
		category.setNumber(number);//设置number
		categoryDao.add(category);//添加到数据库

	}

//	编辑
	public void editService(String edit,String originalName) {

//		通过name查询获取
		Category category = new CategoryDao().get(originalName);

		category.setId(category.getId());//设置id
		category.setName(edit);//设置name
		category.setNumber(category.getNumber());//设置number
		categoryDao.update(category);//更新数据库中对应数据

	}

//	删除
	public void deleteService(String delete){

//		通过name查询获取
		Category category = new CategoryDao().get(delete);

		categoryDao.delete(category.getId());//删除数据库中对应数据

	}
}
