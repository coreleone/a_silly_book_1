/**
 * @File:        RecordService.java
 * @Package:     service
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午10:34:54
 */

package service;

import java.sql.Date;

import dao.RecordDao;
import entity.Record;

/**
 * @ClassName:   RecordService
 * @Description: 记一笔数据服务
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午10:34:54
 */
public class RecordService {
//	饿汉式单例模式
	private RecordService() {

	}
	private static RecordService instance = new RecordService();
	public static RecordService getInstance() {
		if(null == instance) {
			instance = new RecordService();
		}

		return instance;
	}

//	记一笔的Entity类和DAO类
	public Record record = new Record();
	public RecordDao recordDao = new RecordDao();
//	添加
	public void addService(Integer spend,Integer cid,String note,Date date) {
//		设置花费、分类、备注与日期
		record.setSpend(spend);
		record.setcid(cid);
		record.setComment(note);
		record.setDate(date);

		recordDao.add(record);//添加到数据库
	}
}
