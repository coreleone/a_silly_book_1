/**
 * @File:        ChartUtil.java
 * @Package:     gui.panel
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午5:11:18
 */

package util;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StackedXYBarRenderer;
import org.jfree.data.time.TimeTableXYDataset;

import gui.panel.RecordPanel;
import service.ReportService;

/**
 * @ClassName:   ChartUtil
 * @Description: 月消费报告 图标工具类
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午5:11:18
 */
public class ChartUtil {
//	图表面板
	ChartPanel chartPanel;
//	数据集与JFreeChart
	TimeTableXYDataset data = ReportService.getInstance().getDataSet();
	JFreeChart chart;

//	无参构造器
	public ChartUtil() {
//		X轴 时间轴
		DateAxis dateAxis = new DateAxis(null);
		SimpleDateFormat sdf = new SimpleDateFormat("d日");
		//格式化时间轴显示
		//隔多少个单位显示
		dateAxis.setAutoTickUnitSelection(false);
		dateAxis.setTickUnit(new DateTickUnit(DateTickUnit.DAY,5,sdf));

//		Y轴 数值轴
		NumberAxis numberAxis = new NumberAxis(null);
		numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		//格式化数值轴显示
		//隔多少个单位显示
		numberAxis.setAutoTickUnitSelection(false);
		numberAxis.setTickUnit(new NumberTickUnit(10));

//		柱形图显示
		StackedXYBarRenderer stackedXYBarRenderer = new StackedXYBarRenderer();
//		stackedXYBarRenderer.setBaseItemLabelsVisible(true);
//		stackedXYBarRenderer.setItemLabelAnchorOffset(0);
//		stackedXYBarRenderer.setBaseItemLabelGenerator(new StandardXYItemLabelGenerator());
//		stackedXYBarRenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12,TextAnchor.BOTTOM_CENTER));
		stackedXYBarRenderer.setMargin(0.1);

//		获取Plot
		XYPlot xyplot = new XYPlot(data,dateAxis,numberAxis,stackedXYBarRenderer);

//		JFreeChart
		chart = new JFreeChart(null,xyplot);
//		chart.setAntiAlias(false);//必须设置文本抗锯齿为false,防止乱码

//		获得图表区域对象
		XYPlot plot = chart.getXYPlot();

//		字体设置
//		水平
		ValueAxis domain = plot.getDomainAxis();
//		水平底部标题设置
		domain.setLabel("日期");
		domain.setLabelFont(new Font("黑体",Font.PLAIN,12));
		domain.setTickMarkPaint(ChartColor.BLACK);
//		垂直
		ValueAxis rangeAxis = plot.getRangeAxis();
//		垂直标题字体设置
		rangeAxis.setLabel("消费金额（￥）");
		rangeAxis.setLabelFont(new Font("黑体",Font.PLAIN,12));
		rangeAxis.setTickMarkPaint(ChartColor.BLACK);
//		设置legend字体
		chart.getLegend().setItemFont(new Font("黑体",Font.PLAIN,12));

//		区域设置
		plot.setBackgroundPaint(ChartColor.WHITE);//背景颜色
		plot.setOutlinePaint(ChartColor.BLACK);//边框颜色
//		plot.setDomainGridlinePaint(ChartColor.BLACK);//X轴网格线
//		plot.setRangeGridlinePaint(ChartColor.BLACK);//Y轴网格线

//		初始化ChartPanel
		chartPanel = new ChartPanel(chart);
	}

//	获取ChartPanel方法
	public ChartPanel getChartPanel() {
		return chartPanel;
	}

//	数据更新线程
	public class reportDataUpdate implements Runnable{
		public void run() {
//			记一笔按钮 添加监听
			RecordPanel.getInstance().bSubmit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
//					数据更新
					TimeTableXYDataset data = ReportService.getInstance().getDataSet();
					XYPlot plot = chart.getXYPlot();
					plot.setDataset(data);
				}
			});
		}
	}

//
}
