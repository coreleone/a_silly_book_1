/**
 * @File:        MysqlUtil.java
 * @Package:     util
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月4日 下午11:37:57
 */

package util;

import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @ClassName:   MysqlUtil
 * @Description: Mysql工具类
 * @Author:    	 橙子
 * @Date:        2021年1月4日 下午11:37:57
 */
public class MysqlUtil {
//	获取数据库配置信息
	public Map<String,String> readProperties(){
//		数据库配置文件相对路径
		String propertiesPath = "/util/jdbc.properties";
		/*Properties类表示了一个持久的属性集。
		Properties可保存在流中或从流中加载。
		属性列表中每个键及其对应值都是一个字符串。*/
		Properties properties = new Properties();
//		用Map接收
		Map<String,String> configuration = new HashMap<String,String>();
		InputStreamReader isr = null;
		try {
			isr = new InputStreamReader(new MysqlUtil().getClass().getResourceAsStream(propertiesPath),"UTF-8");
			properties.load(isr);
//			获取properties的枚举，并将其中数据添加到Map中
			Enumeration en = properties.propertyNames();
			while(en.hasMoreElements()) {
				String key = (String)en.nextElement();
				String value = properties.getProperty(key);
				configuration.put(key, value);
			}
			return configuration;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
