/**
 * @File:        CircleProgressBar.java
 * @Package:     gui.panel
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午10:09:55
 */

package util;

import javax.swing.JProgressBar;

/**
 * @ClassName:   CircleProgressBar
 * @Description: 环形进度条
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午10:09:55
 */
public class CircleProgressBar{
//	TODO: 环形进度条，未完成
//	进度条
	public JProgressBar progressBar;

	public JProgressBar getProgressBar() {
		progressBar = new JProgressBar(0,100);

		progressBar.setBounds(50, 10, 470-200, 470-400);

		return progressBar;
	}
}
