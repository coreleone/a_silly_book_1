/**
 * @File:        DBUtil.java
 * @Package:     util
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月31日 下午7:07:27
 */

package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @ClassName:   DBUtil
 * @Description: 数据库连接工具类
 * @Author:    	 橙子
 * @Date:        2020年12月31日 下午7:07:27
 */
public class DBUtil {
	public static Connection getConnection(){
		try {
//			获取Mysql连接
			return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/hutubill?characterEncoding=UTF-8","root","admin");
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
