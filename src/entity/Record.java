/**
 * @File:        Record.java
 * @Package:     entity
 * @Project:     a_silly_book
 * @Description: 
 * @Author:    	 橙子
 * @Date:        2020年12月31日 下午6:56:01
 */

package entity;

import java.sql.Date;

/**
 * @ClassName:   Record
 * @Description: 消费分类Record类
 * @Author:    	 橙子
 * @Date:        2020年12月31日 下午6:56:01
 */
public class Record {
	private int id;
	private int spend;
	private int cid;
	private String comment;
	private Date date;

	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}

	public void setSpend(int spend) {
		this.spend = spend;
	}
	public int getSpend() {
		return spend;
	}

	public void setcid(int cid) {
		this.cid = cid;
	}
	public int getcid() {
		return cid;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getComment() {
		return comment;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	public Date getDate() {
		return date;
	}
}
