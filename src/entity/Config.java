/**
 * @File:        Config.java
 * @Package:     entity
 * @Project:     a_silly_book
 * @Description: 
 * @Author:    	 橙子
 * @Date:        2020年12月31日 下午6:43:58
 */

package entity;

/**
 * @ClassName:   Config
 * @Description: 配置信息Config类
 * @Author:    	 橙子
 * @Date:        2020年12月31日 下午6:43:58
 */
public class Config {
	private int id;
	private String key;
	private String value;

	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}

	public void setKey(String key) {
		this.key = key;
	}
	public String getKey() {
		return key;
	}

	public void setValue(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
}
