/**
 * @File:        Category.java
 * @Package:     entity
 * @Project:     a_silly_book
 * @Description: 
 * @Author:    	 橙子
 * @Date:        2020年12月31日 下午6:50:51
 */

package entity;

/**
 * @ClassName:   Category
 * @Description: 消费分类Category类
 * @Author:    	 橙子
 * @Date:        2020年12月31日 下午6:50:51
 */
public class Category {
	private int id;
	private String name;
	private String number;

	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	public String getNumber() {
		return number;
	}

}
