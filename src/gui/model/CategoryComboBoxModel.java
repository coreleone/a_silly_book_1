/**
 * @File:        CategoryComboBoxModel.java
 * @Package:     gui.panel
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午11:02:15
 */

package gui.model;

import javax.swing.JComboBox;

/**
 * @ClassName:   CategoryComboBoxModel
 * @Description: 消费分类选择框
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午11:02:15
 */
public class CategoryComboBoxModel{
	public JComboBox<String> comboBox;//组合框

//	饿汉式单例模式
	private CategoryComboBoxModel() {
		comboBox = new JComboBox<String>();//组合框
	}
	private static CategoryComboBoxModel instance = new CategoryComboBoxModel();
	public static CategoryComboBoxModel getInstance() {
		if(null == instance) {
			instance = new CategoryComboBoxModel();
		}

		return instance;
	}
}
