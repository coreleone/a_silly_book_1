/**
 * @File:        CategoryTabelModel.java
 * @Package:     gui.model
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午11:35:12
 */

package gui.model;

import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import dao.CategoryDao;
import entity.Category;

/**
 * @ClassName:   CategoryTabelModel
 * @Description: 消费分类表格
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午11:35:12
 */
public class CategoryTableModel {
	public JTable table;//表格
	public Vector<Vector<String>> rowData;//行数据
	public Vector<String> columnName;//列名称
	public Vector<String> line1Data;//一行数据

//	饿汉式单例模式
	private CategoryTableModel() {
//		列名称
		columnName = new Vector<String>();
		columnName.add("分类名称");
		columnName.add("消费次数");

//		行数据
		rowData = new Vector<Vector<String>>();

		List<Category> categorys = new CategoryDao().list();
		if(!categorys.isEmpty()) {
			for(Category category:categorys) {
				line1Data = new Vector<String>();
				line1Data.add(category.getName());
				line1Data.add(category.getNumber());
				rowData.add(line1Data);
			}
		}

		table = new JTable(rowData,columnName);//表格

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setDragEnabled(false);
//		table.setEnabled(false);
	}
	private static CategoryTableModel instance = new CategoryTableModel();
	public static CategoryTableModel getInstance() {
		if(null == instance) {
			instance = new CategoryTableModel();
		}

		return instance;
	}
}
