/**
 * @Project:     a_silly_book
 * @Package      gui.frame
 * @File:        MainFrame.java
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月13日 下午5:12:03
 */

package gui.frame;

import javax.swing.JFrame;

import gui.listener.BackupListener;
import gui.listener.CategoryListener;
import gui.listener.ConfigListener;
import gui.listener.RecordListener;
import gui.listener.RecoverListener;
import gui.listener.ToolBarListener;
import gui.panel.MainPanel;
import gui.panel.SpendPanel;

/**
 * @ClassName:   MainFrame
 * @Description: gui主框架
 * @Author:    	 橙子
 * @Date:        2020年12月13日 下午5:12:03
 */
public class MainFrame{
	public JFrame mainFrame;//主框架
	public static int mainFrameWidth = 470;//主框架宽度
	public static int mainFrameHeight = 470;//主框架高度

//	饿汉式单例模式
	private MainFrame() {
		mainFrame = new JFrame();//主框架新建对象

//		设置主框架参数
		mainFrame.setTitle("一本糊涂账");
		mainFrame.setSize(mainFrameWidth,mainFrameHeight);
		mainFrame.setResizable(false);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

//		将消费一览面板添加到主面板上的空面板
		MainPanel.getInstance().blankPanel.add(SpendPanel.getInstance().spendPanel);
//		将主面板添加到主框架上
		mainFrame.add(MainPanel.getInstance().mainPanel);

//		工具栏按钮添加监听
		ToolBarListener.getInstance().addListener();

//		记一笔面板添加监听器
		RecordListener.getInstance().addListener();
//		消费分类面板添加监听
		CategoryListener.getInstance().addListener();
//		设置面板添加监听
		ConfigListener.getInstance().addListener();
//		备份面板添加监听
		BackupListener.getInstance().addListener();
//		恢复面板添加监听
		RecoverListener.getInstance().addListener();

	}
	private static MainFrame instance = new MainFrame();
	public static MainFrame getInstance() {
		if(null == instance) {
			instance = new MainFrame();
		}

		return instance;
	}
}