/**
 * @File:        BackupPanel.java
 * @Package:     util
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午5:31:51
 */

package gui.panel;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @ClassName:   BackupPanel
 * @Description: 备份面板
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午5:31:51
 */
public class BackupPanel{
	public JPanel backupPanel;//备份面板
	public static int backupPanelWidth = MainPanel.blankPanelWidth;//备份面板宽度
	public static int backupPanelHeight = MainPanel.blankPanelHeight;//备份面板高度

	//	饿汉式单例模式
	private BackupPanel() {
		backupPanel = new CenterPanel().getUnstretchCenterPanel();//备份面板新建对象

//		设置备份面板参数
		backupPanel.setLayout(new FlowLayout());
		backupPanel.add(center());

	}
	private static BackupPanel instance = new BackupPanel();
	public static BackupPanel getInstance() {
		if(null == instance) {
			instance = new BackupPanel();
		}

		return instance;
	}


	/**
	 * @Title:       center
	 * @Description: 中部面板
	 * @Author: 	 橙子
	 * @param:       @return
	 * @return:      JPanel
	 * @throws:
	 */
	public JButton bBackup = new JButton("备份");
	private JPanel center() {
		JPanel p = new JPanel();
		p.add(bBackup);
		return p;
	}
}
