/**
 * @File:        ConfigPanel.java
 * @Package:     util
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午5:16:25
 */

package gui.panel;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * @ClassName:   ConfigPanel
 * @Description: 设置面板
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午5:16:25
 */
public class ConfigPanel{
	public JPanel configPanel;//设置面板

//	饿汉式单例模式
	private ConfigPanel() {
		configPanel = new CenterPanel().getUnstretchCenterPanel();//设置面板新建对象

//		设置 设置面板参数
		configPanel.setLayout(new BorderLayout());
		configPanel.add(center(),BorderLayout.CENTER);
		configPanel.add(south(),BorderLayout.SOUTH);

	}
	private static ConfigPanel instance = new ConfigPanel();
	public static ConfigPanel getInstance() {
		if(null == instance) {
			instance = new ConfigPanel();
		}

		return instance;
	}

	/**
	 * @Title:       south
	 * @Description: 南部面板
	 * @Author: 	 橙子
	 * @param:       @return
	 * @return:      JPanel
	 * @throws:
	 */
	public JButton bUpdate = new JButton("更新");
	private JPanel south() {
		JPanel p = new JPanel();
		p.setBorder(new EmptyBorder(30,0,0,0));

		p.add(bUpdate);
		return p;
	}

	/**
	 * @Title:       center
	 * @Description: 中间面板
	 * @Author: 	 橙子
	 * @param:       @return
	 * @return:      JPanel
	 * @throws:
	 */
	public JTextField vBudget = new JTextField("",40);
	public JTextField vMysqlContent = new JTextField();
	private JPanel center() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(4,1));

		//获取布局管理器并设置组件间距离
		((GridLayout)p.getLayout()).setHgap(0);
		((GridLayout)p.getLayout()).setVgap(CenterPanel.centerPanelHeight/15);

		p.add(new JLabel("本月预算(￥)"));
		p.add(vBudget);
		p.add(new JLabel("Mysql安装目录"));
		p.add(vMysqlContent);
		return p;
	}
}
