/**
 * @File:        CategoryPanel.java
 * @Package:     gui.panel
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午11:25:43
 */

package gui.panel;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import gui.model.CategoryTableModel;

/**
 * @ClassName:   CategoryPanel
 * @Description: 消费分类面板
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午11:25:43
 */
public class CategoryPanel{
	public JPanel categoryPanel;//消费分类面板

//	饿汉式单例模式
	private CategoryPanel() {
		categoryPanel = new CenterPanel().getUnstretchCenterPanel();//消费分类面板新建对象

//		设置消费分类面板参数
		categoryPanel.setLayout(new BorderLayout());
		categoryPanel.add(center(),BorderLayout.CENTER);
		categoryPanel.add(south(),BorderLayout.SOUTH);

	}
	private static CategoryPanel instance = new CategoryPanel();
	public static CategoryPanel getInstance() {
		if(null == instance) {
			instance = new CategoryPanel();
		}

		return instance;
	}

//	 中部面板
	  public JTable tCategory = CategoryTableModel.getInstance().table;
	  private JPanel center() {
		  JPanel p = new JPanel();
		  p.setLayout(new BorderLayout());
		  JScrollPane scrollPane = new JScrollPane(tCategory);//设置表格可滚动
		  p.add(scrollPane,BorderLayout.CENTER);
		  return p;
	  }

//	  南部面板
	  public JButton bAdd = new JButton("新增");
	  public JButton bEdit = new JButton("编辑");
	  public JButton bDelete = new JButton("删除");
	  private JPanel south() {
		  JPanel p  = new JPanel();
		  p.setBorder(new EmptyBorder(30,0,0,0));

		  p.add(bAdd);
		  p.add(bEdit);
		  p.add(bDelete);
		  return p;
	  }
}
