/**
 * @File:        RecordPanel.java
 * @Package:     gui.panel
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午10:40:23
 */

package gui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.Calendar;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.eltima.components.ui.DatePicker;

import dao.CategoryDao;
import entity.Category;
import gui.model.CategoryComboBoxModel;

/**
 * @ClassName:   RecordPanel
 * @Description: 记一笔面板
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午10:40:23
 */
public class RecordPanel{
	public JPanel recordPanel;//记一笔面板

//	饿汉式单例模式
	private RecordPanel() {
		 recordPanel = new CenterPanel().getUnstretchCenterPanel();//记一笔面板新建对象

//		 设置记一笔面板参数
		 recordPanel.setLayout(new BorderLayout());
		 recordPanel.add(center(),BorderLayout.NORTH);
		 recordPanel.add(south(),BorderLayout.SOUTH);

	}
	private static RecordPanel instance = new RecordPanel();
	public static RecordPanel getInstance() {
		if(null == instance) {
			instance = new RecordPanel();
		}

		return instance;
	}

//	 中部面板
	 public JTextField vSpend = new JTextField("");
	 public JComboBox<String> vCategory = CategoryComboBoxModel.getInstance().comboBox;
	 public JTextField vNote = new JTextField();
	 public DatePicker vDate = new DatePicker();
	 private JPanel center() {
		 JPanel p = new JPanel();
		 p.setLayout(new GridLayout(4,2));

		 vSpend.setToolTipText("请输入花费（￥）");
		 vNote.setToolTipText("请输入备注");

		//获取布局管理器并设置组件间距离
		((GridLayout)p.getLayout()).setHgap(0);
		((GridLayout)p.getLayout()).setVgap(CenterPanel.centerPanelHeight/10);

//		查询数据库中的分类表，并将名称添加到“记一笔”中的分类
		List<Category> categorys = new CategoryDao().list();
		for(Category category : categorys) {
			vCategory.addItem(category.getName());
		}

		Calendar c = Calendar.getInstance();
		vDate.getInnerTextField().setValue(c.getTime());
		vDate.setHightlightdays(new int[] {c.get(Calendar.DAY_OF_MONTH)},Color.YELLOW);

		 p.add(new JLabel("花费（￥）"));
		 p.add(vSpend);
		 p.add(new JLabel("分类"));
		 p.add(vCategory);
		 p.add(new JLabel("备注"));
		 p.add(vNote);
		 p.add(new JLabel("日期"));
		 p.add(vDate);
		 return p;
	 }

//	 南部面板
	 public  JButton bSubmit = new JButton("记一笔");
	 private JPanel south() {
		 JPanel p = new JPanel();
		 p.setBorder(new EmptyBorder(30,0,0,0));

		 p.add(bSubmit);
		 return p;
	 }
}
