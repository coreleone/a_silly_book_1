/**
 * @File:        RecoverPanel.java
 * @Package:     util
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午5:43:40
 */

package gui.panel;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @ClassName:   RecoverPanel
 * @Description: 恢复面板
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午5:43:40
 */
public class RecoverPanel{
	public JPanel recoverPanel;//恢复面板

//	饿汉式单例模式
	private RecoverPanel() {
		recoverPanel = new CenterPanel().getUnstretchCenterPanel();//恢复面板新建对象

//		设置恢复面板参数
		recoverPanel.setLayout(new BorderLayout());
		recoverPanel.add(center(),BorderLayout.CENTER);


	}
	private static RecoverPanel instance = new RecoverPanel();
	public static RecoverPanel getInstance() {
		if(null == instance) {
			instance = new RecoverPanel();
		}

		return instance;
	}

	/**
	 * @Title:       center
	 * @Description: 中部面板
	 * @Author: 	 橙子
	 * @param:       @return
	 * @return:      JPanel
	 * @throws:
	 */
	public JButton bRecover = new JButton("恢复");
	private JPanel center() {
		JPanel p = new JPanel();
		p.add(bRecover);
		return p;
	}
}
