/**
 * @Project:     a_silly_book
 * @Package      gui.panel
 * @File:        MainPanel.java
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月19日 下午9:36:42
 */

package gui.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import gui.frame.MainFrame;

/**
 * @ClassName:   MainPanel
 * @Description: 主面板
 * @Author:    	 橙子
 * @Date:        2020年12月19日 下午9:36:42
 */
public class MainPanel{
	public JPanel mainPanel;//主面板
	public static int mainPanelWidth = MainFrame.mainFrameWidth;//主面板宽度
	public static int mainPanelHeight = MainFrame.mainFrameHeight-30;//主面板高度

//	工具栏与按钮
	JToolBar tb = new JToolBar();
	public static int tbWidth = mainPanelWidth;//工具栏宽度
	public static int tbHeight = 100;//工具栏高度
	public JButton bSpend = new JButton("消费一览");
	public JButton bRecord = new JButton("记一笔");
	public JButton bCategory = new JButton("消费分类");
	public JButton bReport = new JButton("月消费报表");
	public JButton bConfig = new JButton("设置");
	public JButton bBackup = new JButton("备份");
	public JButton bRecover = new JButton("恢复");

//	空白面板
	public JPanel blankPanel = new JPanel();
	public static int blankPanelWidth = mainPanelWidth;//空白面板宽度
	public static int blankPanelHeight = mainPanelHeight-tbHeight;//空白面板高度

//	饿汉式单例模式
	private MainPanel(){
		mainPanel = new JPanel();//主面板新建对象
		mainPanel.setSize(mainPanelWidth,mainPanelHeight);

//		按钮图片
		bSpend.setIcon(new ImageIcon("D:\\programming_learning\\javaWeb_learning_project\\how2jcn$20201211\\practical_project_1\\a_silly_book_1\\消费一览.png"));
		bRecord.setIcon(new ImageIcon("D:\\programming_learning\\javaWeb_learning_project\\how2jcn$20201211\\practical_project_1\\a_silly_book_1\\记一笔.png"));
		bCategory.setIcon(new ImageIcon("D:\\programming_learning\\javaWeb_learning_project\\how2jcn$20201211\\practical_project_1\\a_silly_book_1\\消费分类.png"));
		bReport.setIcon(new ImageIcon("D:\\programming_learning\\javaWeb_learning_project\\how2jcn$20201211\\practical_project_1\\a_silly_book_1\\月消费报表.png"));
		bConfig.setIcon(new ImageIcon("D:\\programming_learning\\javaWeb_learning_project\\how2jcn$20201211\\practical_project_1\\a_silly_book_1\\设置.png"));
		bBackup.setIcon(new ImageIcon("D:\\programming_learning\\javaWeb_learning_project\\how2jcn$20201211\\practical_project_1\\a_silly_book_1\\备份.png"));
		bRecover.setIcon(new ImageIcon("D:\\programming_learning\\javaWeb_learning_project\\how2jcn$20201211\\practical_project_1\\a_silly_book_1\\恢复.png"));

//		按钮文本位置
		bSpend.setVerticalTextPosition(SwingConstants.BOTTOM);
		bSpend.setHorizontalTextPosition(SwingConstants.CENTER);
		bRecord.setVerticalTextPosition(SwingConstants.BOTTOM);
		bRecord.setHorizontalTextPosition(SwingConstants.CENTER);
		bCategory.setVerticalTextPosition(SwingConstants.BOTTOM);
		bCategory.setHorizontalTextPosition(SwingConstants.CENTER);
		bReport.setVerticalTextPosition(SwingConstants.BOTTOM);
		bReport.setHorizontalTextPosition(SwingConstants.CENTER);
		bConfig.setVerticalTextPosition(SwingConstants.BOTTOM);
		bConfig.setHorizontalTextPosition(SwingConstants.CENTER);
		bBackup.setVerticalTextPosition(SwingConstants.BOTTOM);
		bBackup.setHorizontalTextPosition(SwingConstants.CENTER);
		bRecover.setVerticalTextPosition(SwingConstants.BOTTOM);
		bRecover.setHorizontalTextPosition(SwingConstants.CENTER);

//		将按钮添加到工具栏
		tb.add(bSpend);
		tb.add(bRecord);
		tb.add(bCategory);
		tb.add(bReport);
		tb.add(bConfig);
		tb.add(bBackup);
		tb.add(bRecover);

//		设置工具栏参数
		tb.setFloatable(false);
		tb.setPreferredSize(new Dimension(tbWidth,tbHeight));

//		设置空白面板参数
		blankPanel.setPreferredSize(new Dimension(blankPanelHeight,blankPanelHeight));

//		设置主面板参数
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(tb,BorderLayout.NORTH);
		mainPanel.add(blankPanel,BorderLayout.SOUTH);
	}
	private static MainPanel instance = new MainPanel();
	public static MainPanel getInstance() {
		if(null == instance) {
			instance = new MainPanel();
		}

		return instance;
	}
}
