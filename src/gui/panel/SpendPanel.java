/**
 * @File:        SpendPanel.java
 * @Package:     gui.panel
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午9:19:43
 */

package gui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import gui.frame.MainFrame;
import service.SpendService;
import util.CircleProgressBar;

/**
 * @ClassName:   SpendPanel
 * @Description: 消费一览面板
 * @Author:    	 橙子
 * @Date:        2020年12月30日 上午9:19:43
 */
public class SpendPanel{
	public JPanel spendPanel;//消费一览面板

//	饿汉式单例模式
	private SpendPanel() {
		spendPanel = new CenterPanel().getUnstretchCenterPanel();//消费一览面板新建对象

//		点击时更新数据
		this.updateSpendPanel();

//		设置消费一览面板参数
		spendPanel.setLayout(new BorderLayout());
		spendPanel.add(center(),BorderLayout.CENTER);
		spendPanel.add(south(),BorderLayout.SOUTH);

	}
	private static SpendPanel instance = new SpendPanel();
	public static SpendPanel getInstance() {
		if(null == instance) {
			instance = new SpendPanel();
		}

		return instance;
	}

	public void updateSpendPanel() {
//		点击时更新数据
		SpendService.getInstance().getSpendPanelData();

//		添加到消费一览面板上
		vMonthSpend.setText("￥"+SpendService.getInstance().monthSpend);
		vDaySpend.setText("￥"+SpendService.getInstance().daySpend);

//		添加到消费一览面板上
		vAvgSpendPerDay.setText("￥"+SpendService.getInstance().avgSpendPerDay);
		vRestofThisMonth.setText("￥"+SpendService.getInstance().restofThisMonth);
		vAvgDailyAvailable.setText("￥"+SpendService.getInstance().avgDailyAvailable);
		vMonthLeftDay.setText(SpendService.getInstance().monthLeftDay+"天");

//		进度条与剩余比例
		int monthSpend = SpendService.getInstance().monthSpend;
		int restofThisMonth = SpendService.getInstance().restofThisMonth;
		progressBar.setBackground(this.green);
		progressBar.setForeground(this.blue);
		vRestRate.setForeground(this.green);
		int budget = monthSpend+restofThisMonth;
		if(budget != 0) {
			int spend = (int)Math.round((double)monthSpend/budget*100);
			progressBar.setValue(spend);
			vRestRate.setText("剩余:"+(100-spend)+"%");
		}else {
			progressBar.setValue(0);
			vRestRate.setText("剩余："+0);
		}
	}

//	中部面板
	private JPanel center() {
		JPanel p = new JPanel();
		p.setPreferredSize(new Dimension(CenterPanel.centerPanelWidth,CenterPanel.centerPanelHeight-100));
//		p.setBorder(new EmptyBorder(0,0,0,0));
		p.setLayout(new BorderLayout());

		p.add(west(),BorderLayout.WEST);
		p.add(center2(),BorderLayout.CENTER);
		return p;
	}

	public JLabel vMonthSpend = new JLabel("￥0");
	public JLabel vDaySpend = new JLabel("￥0");
	public Color blue = new Color(51, 153, 255);
	public Color green = new Color(132, 174, 51);
	private JPanel west() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(4,1));

		//获取布局管理器并设置组件间距离
		((GridLayout)p.getLayout()).setHgap(10);
		((GridLayout)p.getLayout()).setVgap(CenterPanel.centerPanelWidth/15);

		vMonthSpend.setForeground(this.blue);
		vDaySpend.setForeground(this.blue);

		p.add(new JLabel("本月消费"));
		p.add(vMonthSpend);
		p.add(new JLabel("今日消费"));
		p.add(vDaySpend);

		return p;
	}

	public JProgressBar progressBar = new CircleProgressBar().getProgressBar();//进度条
	public JLabel vRestRate = new JLabel("0%");//剩余比例
	private JPanel center2() {
		JPanel p = new JPanel();
		p.setLayout(null);

		Font bigFont = new Font("bigFont",Font.BOLD,30);
		vRestRate.setLocation(110, 40);
		vRestRate.setSize(MainFrame.mainFrameWidth-300, MainFrame.mainFrameHeight-300);
		vRestRate.setFont(bigFont);

		p.add(progressBar);
		p.add(vRestRate);

		return p;
	}

//	南部面板
	public JLabel vAvgSpendPerDay = new JLabel("￥0");
	public JLabel vRestofThisMonth = new JLabel("￥0");
	public JLabel vAvgDailyAvailable = new JLabel("￥0");
	public JLabel vMonthLeftDay = new JLabel("30天");
	private JPanel south() {
		JPanel p = new JPanel();
		p.setPreferredSize(new Dimension(CenterPanel.centerPanelWidth,100));
		p.setBorder(new EmptyBorder(30,0,0,0));
		p.setLayout(new GridLayout(2,4));

		//获取布局管理器并设置组件间距离
		((GridLayout)p.getLayout()).setHgap(CenterPanel.centerPanelWidth/8);
		((GridLayout)p.getLayout()).setVgap(0);

		vAvgSpendPerDay.setForeground(this.blue);
		vRestofThisMonth.setForeground(this.blue);
		vAvgDailyAvailable.setForeground(this.blue);
		vMonthLeftDay.setForeground(this.blue);

		p.add(new JLabel("日均消费"));
		p.add(new JLabel("本月剩余"));
		p.add(new JLabel("日均可用"));
		p.add(new JLabel("距离月末"));
		p.add(vAvgSpendPerDay);
		p.add(vRestofThisMonth);
		p.add(vAvgDailyAvailable);
		p.add(vMonthLeftDay);
		return p;
	}
}
