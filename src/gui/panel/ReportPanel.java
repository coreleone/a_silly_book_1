/**
 * @File:        ReportPanel.java
 * @Package:     gui.panel
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午2:07:42
 */

package gui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;

import util.ChartUtil;

/**
 * @ClassName:   ReportPanel
 * @Description: 月消费报告面板
 * @Author:    	 橙子
 * @Date:        2020年12月30日 下午2:07:42
 */
public class ReportPanel{
	public JPanel reportPanel;//月消费报告面板

//	饿汉式单例模式
	private ReportPanel() {
		reportPanel = new CenterPanel().getUnstretchCenterPanel();//月消费报告新建对象

//		设置月消费报告面板参数
		reportPanel.setLayout(new BorderLayout());
		reportPanel.add(west(),BorderLayout.WEST);
		reportPanel.add(center(),BorderLayout.CENTER);
	}
	private static ReportPanel instance = new ReportPanel();
	public static ReportPanel getInstance() {
		if(null == instance) {
			instance = new ReportPanel();
		}

		return instance;
	}

	/**
	 * @Title:       center
	 * @Description: 中部面板
	 * @Author: 	 橙子
	 * @param:       @return
	 * @return:      JPanel
	 * @throws:
	 */
	public ChartPanel chartPanel = new ChartUtil().getChartPanel();
	private JPanel center() {
		JPanel p = new JPanel();
		p.setLayout(new BorderLayout());
		chartPanel.setPreferredSize(new Dimension(300,300));
		p.add(chartPanel);
		return p;
	}

	/**
	 * @Title:       west
	 * @Description: 西部面板
	 * @Author: 	 橙子
	 * @param:       @return
	 * @return:      JPanel
	 * @throws:
	 */
	private JPanel west() {
		JPanel p = new JPanel();
		p.setLayout(new FlowLayout());
		JLabel colorLabel = new JLabel("");
		colorLabel.setBackground(Color.red);
		colorLabel.setOpaque(true);
		colorLabel.setPreferredSize(new Dimension(15,15));
		p.add(colorLabel,BorderLayout.WEST);
		p.add(new JLabel("月消费报表"),BorderLayout.CENTER);
		return p;
	}


}
