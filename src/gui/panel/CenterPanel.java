/**
 * @File:        CenterPanel.java
 * @Package:     gui.panel
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月6日 下午2:55:34
 */

package gui.panel;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * @ClassName:   CenterPanel
 * @Description: 居中面板
 * @Author:    	 橙子
 * @Date:        2021年1月6日 下午2:55:34
 */
public class CenterPanel{

	public static int centerPanelWidth = MainPanel.blankPanelWidth;//中间面板宽度
	public static int centerPanelHeight = MainPanel.blankPanelHeight;//中间面板高度

//	获取非拉伸居中面板
	public JPanel getUnstretchCenterPanel() {
		JPanel centerPanel = new JPanel();//非拉伸居中面板

		centerPanel.setBorder(new EmptyBorder(30,30,30,30));//设置边界距离
		centerPanel.setPreferredSize(new Dimension(CenterPanel.centerPanelWidth,CenterPanel.centerPanelHeight));//设置优先尺寸

		return centerPanel;//返回centerPanel
	}
}
