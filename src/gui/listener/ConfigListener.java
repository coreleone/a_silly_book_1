/**
 * @File:        ConfigListener.java
 * @Package:     gui.listener
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午5:25:46
 */

package gui.listener;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JOptionPane;

import dao.ConfigDao;
import entity.Config;
import gui.panel.ConfigPanel;
import gui.panel.SpendPanel;
import service.ConfigService;
import service.SpendService;

/**
 * @ClassName:   ConfigListener
 * @Description: 设置监听
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午5:25:46
 */
public class ConfigListener {
//	饿汉式单例模式
	private ConfigListener() {

	}
	private static ConfigListener instance = new ConfigListener();
	public static ConfigListener getInstance() {
		if(null == instance) {
			instance = new ConfigListener();
		}

		return instance;
	}
//	方法：添加监听器
	public void addListener() {
//		更新按钮 添加监听
		ConfigPanel.getInstance().bUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				获取预算和Mysql安装路径
				String budget = ConfigPanel.getInstance().vBudget.getText();
				String mysqlContent = ConfigPanel.getInstance().vMysqlContent.getText();
				if((budget != null && budget.matches("\\d+")) && (mysqlContent != null && mysqlContent.matches("[a-zA-Z]:\\\\.+"))) {
//					添加到数据库中
					ConfigService.getInstance().updateService(budget,mysqlContent);

					JOptionPane.showMessageDialog(ConfigPanel.getInstance().configPanel, "更新成功");

//					更新消费一览面板
					this.updateSpendPanel();
				}else {
					JOptionPane.showMessageDialog(ConfigPanel.getInstance().configPanel, "请正确输入，预算为零或者正整数，目录可直接从电脑复制填写");
//					保持文本存在
					Config config = new ConfigDao().getTheLastOne();
					ConfigPanel configPanel = ConfigPanel.getInstance();
					if(!budget.matches("\\d+")) {
						configPanel.vBudget.setText(config.getKey());
					}
					if(!mysqlContent.matches("[a-zA-Z]:\\\\.+")) {
						configPanel.vMysqlContent.setText(config.getValue());
					}
				}
			}

			public void updateSpendPanel() {
//				更新消费一览面板

//				获取日历
				Calendar cal = Calendar.getInstance();
//				本日
				int currentDay = cal.get(Calendar.DAY_OF_MONTH);
//				本月多少日
				int allDayOfThisMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

//				本月剩余与日均可用
				Config config = new ConfigDao().getTheLastOne();
				if(config != null) {
					int budgetCount = Integer.parseInt(config.getKey());
					SpendService.getInstance().restofThisMonth = budgetCount-SpendService.getInstance().monthSpend;
					SpendService.getInstance().avgDailyAvailable = SpendService.getInstance().restofThisMonth/(allDayOfThisMonth-currentDay);
				}else {
					SpendService.getInstance().restofThisMonth = -SpendService.getInstance().monthSpend;
					SpendService.getInstance().avgDailyAvailable = 0;
				}

//				添加到消费一览面板
				SpendPanel.getInstance().vRestofThisMonth.setText("￥"+SpendService.getInstance().restofThisMonth);
				SpendPanel.getInstance().vAvgDailyAvailable.setText("￥"+SpendService.getInstance().avgDailyAvailable);

//				进度条与剩余比例
				int monthSpend = SpendService.getInstance().monthSpend;
				int restofThisMonth = SpendService.getInstance().restofThisMonth;
				SpendPanel.getInstance().progressBar.setBackground(Color.GREEN);
				SpendPanel.getInstance().progressBar.setForeground(Color.BLUE);
				SpendPanel.getInstance().vRestRate.setForeground(Color.GREEN);
				int realBudgetCount = monthSpend+restofThisMonth;
				if(realBudgetCount != 0) {
					int spend = (int)Math.round((double)monthSpend/realBudgetCount*100);
					SpendPanel.getInstance().progressBar.setValue(spend);
					SpendPanel.getInstance().vRestRate.setText("剩余:"+(100-spend)+"%");
				}else {
					SpendPanel.getInstance().progressBar.setValue(100);
					SpendPanel.getInstance().vRestRate.setText("剩余："+0);
				}
			}
		});
	}
}
