/**
 * @File:        RecoverListener.java
 * @Package:     gui.listener
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月5日 上午1:00:49
 */

package gui.listener;

import java.awt.Component;
import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileSystemView;

import dao.ConfigDao;
import entity.Config;
import gui.frame.MainFrame;
import gui.panel.BackupPanel;
import gui.panel.ConfigPanel;
import gui.panel.MainPanel;
import gui.panel.RecoverPanel;
import util.MysqlUtil;

/**
 * @ClassName:   RecoverListener
 * @Description: 恢复监听
 * @Author:    	 橙子
 * @Date:        2021年1月5日 上午1:00:49
 */
public class RecoverListener {
//	饿汉式单例模式
	private RecoverListener() {

	}
	private static RecoverListener instance = new RecoverListener();
	public static RecoverListener getInstance() {
		if(null == instance) {
			instance = new RecoverListener();
		}

		return instance;
	}

//	添加监听
	public void addListener() {
//		备份按钮添加监听
		RecoverPanel.getInstance().bRecover.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
//				获取设置Dao
				ConfigDao configDao = new ConfigDao();
//				获取最新一条数据
				Config config = configDao.getTheLastOne();
				if(config == null) {
//					未设置Mysql安装目录
					JOptionPane.showMessageDialog(BackupPanel.getInstance().backupPanel,"请设置Mysql安装目录！");
//					跳出设置面板
					for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
						c.setVisible(false);
					}
					MainPanel.getInstance().blankPanel.add(ConfigPanel.getInstance().configPanel);
					ConfigPanel.getInstance().configPanel.setVisible(true);
				}else{
					try {
//						导入文件窗口
						FileDialog loadDialog = new FileDialog(MainFrame.getInstance().mainFrame,"备份",FileDialog.LOAD);
//						设置默认目录与默认名称
						loadDialog.setDirectory(FileSystemView.getFileSystemView().getHomeDirectory().toString());
						loadDialog.setFile("hutubill.sql");
						loadDialog.addWindowListener(new WindowAdapter(){
//							取消或者关闭时动作
							public void windowClosed(WindowEvent e) {
								loadDialog.dispose();
							}
						});
//						窗口可视
						loadDialog.setVisible(true);
//						获取选择目录和选择名称
						String dirPath = loadDialog.getDirectory();
						String fileName = loadDialog.getFile();
						if(dirPath == null && fileName == null) {
//							如果取消或者关闭
							return;
						}else if(dirPath == null || fileName == null || !fileName.matches(".+\\.sql")) {
//							如果目录或者名称为空，则提醒并空返回
							JOptionPane.showMessageDialog(BackupPanel.getInstance().backupPanel, "导入失败！未正确设置导入目录或者文件");
							return;
						}else {
//							用户
							String user = new MysqlUtil().readProperties().get("jdbc.user");
//							密码
							String password = new MysqlUtil().readProperties().get("jdbc.password");
//							主机
							String host = new MysqlUtil().readProperties().get("jdbc.host");
//							数据库
							String DatabaseName = new MysqlUtil().readProperties().get("jdbc.DatabaseName");

//							导入文件路径
							File loadFile = new File(dirPath,fileName);
//							Mysql安装目录
							String mysqlPath = config.getValue()+"/bin/";
//							获取cmd命令行窗口
							Runtime runtime = Runtime.getRuntime();
//							导出sql文件命令行
							String command = new String("cmd /c "+mysqlPath+"mysql -h"+host+" -u"+user+" -p"+password+" "+DatabaseName+" >"+loadFile.toString());
//							执行命令
							Process process = runtime.exec(command);

							JOptionPane.showMessageDialog(RecoverPanel.getInstance().recoverPanel, "备份成功");
						}
					}catch(IOException ioe) {
						ioe.printStackTrace();
					}
				}
			}

		});
	}
}
