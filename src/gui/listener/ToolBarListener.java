/**
 * @File:        ToolBarListener.java
 * @Package:     gui.listener
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午4:09:22
 */

package gui.listener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.TimeTableXYDataset;

import dao.CategoryDao;
import dao.ConfigDao;
import entity.Category;
import entity.Config;
import gui.panel.BackupPanel;
import gui.panel.CategoryPanel;
import gui.panel.ConfigPanel;
import gui.panel.MainPanel;
import gui.panel.RecordPanel;
import gui.panel.RecoverPanel;
import gui.panel.ReportPanel;
import gui.panel.SpendPanel;
import service.ReportService;

/**
 * @ClassName:   ToolBarListener
 * @Description: 工具栏监听
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午4:09:22
 */
public class ToolBarListener {
//	饿汉式单例模式
	private ToolBarListener() {

	}
	private static ToolBarListener instance = new ToolBarListener();
	public static ToolBarListener getInstance() {
		if(null == instance) {
			instance = new ToolBarListener();
		}

		return instance;
	}

	public void addListener() {
//		点击消费一览
		MainPanel.getInstance().bSpend.addActionListener(new ActionListener(){
//			获取SpendPanel
			SpendPanel spendPanel = SpendPanel.getInstance();

			public void actionPerformed(ActionEvent e) {
				for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
//					主面板的空面板上的所有组件消失
					c.setVisible(false);
				}

//				将消费一览面板添加到主面板的空面板上，并设置可视
				MainPanel.getInstance().blankPanel.add(spendPanel.spendPanel);
				SpendPanel.getInstance().spendPanel.setVisible(true);

			}
		});

//		点击记一笔
		MainPanel.getInstance().bRecord.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
					c.setVisible(false);
				}
				MainPanel.getInstance().blankPanel.add(RecordPanel.getInstance().recordPanel);
				RecordPanel.getInstance().recordPanel.setVisible(true);
			}
		});

//		点击消费分类
		MainPanel.getInstance().bCategory.addActionListener(new ActionListener(){
//			获取消费分类面板
			CategoryPanel categoryPanel = CategoryPanel.getInstance();

			public void actionPerformed(ActionEvent e) {
				for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
					c.setVisible(false);
				}

//				消费分类表格数据更新
				this.updateCategory();

//				将消费分类面板添加到主面板的空面板上
				MainPanel.getInstance().blankPanel.add(categoryPanel.categoryPanel);
//				消费分类面板可视
				CategoryPanel.getInstance().categoryPanel.setVisible(true);
			}

			public void updateCategory() {
//				消费分类表格数据更新

//				消费分类表格原数据全部清除
				((DefaultTableModel)categoryPanel.tCategory.getModel()).setRowCount(0);

	//			获取表格数据模型
				DefaultTableModel tableModel = (DefaultTableModel)categoryPanel.tCategory.getModel();
	//			获取数据库中的数据
				Vector<String> line1Data;//一行数据
				List<Category> categorys = new CategoryDao().list();//获取数据库中全部Category对象
	//			添加到表格中
				if(!categorys.isEmpty()) {
					for(Category category : categorys) {
						line1Data = new Vector<String>();
						line1Data.add(category.getName());
						line1Data.add(category.getNumber());
	//					添加到表格中
						tableModel.addRow(line1Data);
					}
				}
			}
		});

//		点击月消费报表
		MainPanel.getInstance().bReport.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
					c.setVisible(false);
				}
//				点击时更新数据
				TimeTableXYDataset data = ReportService.getInstance().getDataSet();
				XYPlot plot = ReportPanel.getInstance().chartPanel.getChart().getXYPlot();
				plot.setDataset(data);

				MainPanel.getInstance().blankPanel.add(ReportPanel.getInstance().reportPanel);
				ReportPanel.getInstance().reportPanel.setVisible(true);

			}
		});

//		点击设置
		MainPanel.getInstance().bConfig.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
					c.setVisible(false);
				}
				ConfigPanel configPanel = ConfigPanel.getInstance();

//				把数据库中最新的预算与目录添加到文本框中
				Config config = new ConfigDao().getTheLastOne();
				if(config != null) {
					String budget = config.getKey();
					String mysqlContent = config.getValue();
					configPanel.vBudget.setText(budget);
					configPanel.vMysqlContent.setText(mysqlContent);
				}

				MainPanel.getInstance().blankPanel.add(configPanel.configPanel);
				ConfigPanel.getInstance().configPanel.setVisible(true);
			}
		});

//		点击备份
		MainPanel.getInstance().bBackup.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
					c.setVisible(false);
				}
				MainPanel.getInstance().blankPanel.add(BackupPanel.getInstance().backupPanel);
				BackupPanel.getInstance().backupPanel.setVisible(true);
			}
		});

//		点击恢复
		MainPanel.getInstance().bRecover.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
					c.setVisible(false);
				}
				MainPanel.getInstance().blankPanel.add(RecoverPanel.getInstance().recoverPanel);
				RecoverPanel.getInstance().recoverPanel.setVisible(true);
			}
		});
	}

}
