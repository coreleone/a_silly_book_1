/**
 * @File:        RecordListener.java
 * @Package:     gui.listener
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午10:37:01
 */

package gui.listener;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Calendar;

import javax.swing.JOptionPane;

import dao.CategoryDao;
import dao.ConfigDao;
import entity.Category;
import entity.Config;
import gui.panel.CategoryPanel;
import gui.panel.MainPanel;
import gui.panel.RecordPanel;
import gui.panel.SpendPanel;
import service.RecordService;
import service.SpendService;

/**
 * @ClassName:   RecordListener
 * @Description: 记一笔监听
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午10:37:01
 */
public class RecordListener {
//	饿汉式单例模式
	private RecordListener() {

	}
	private static RecordListener instance = new RecordListener();
	public static RecordListener getInstance() {
		if(null == instance) {
			instance = new RecordListener();
		}

		return instance;
	}

//	花费、分类、备注与日期
	public Integer spend = null;
	public Integer cid = null;
	public String note = null;
	public Date date = null;

//	方法：添加监听器
	public void addListener() {
//		记一笔按钮 添加监听器
		RecordPanel.getInstance().bSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				花费
				try {
					spend = Integer.valueOf(RecordPanel.getInstance().vSpend.getText());
				}catch(Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null, "花费不是零或者正整数，请正确输入");
				}

//				分类
				String selectedItem = (String)RecordPanel.getInstance().vCategory.getSelectedItem();//获取选择的项目

//				如果分类为空
				if(selectedItem == null) {
					JOptionPane.showMessageDialog(RecordPanel.getInstance().recordPanel, "未添加分类");

					for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
						c.setVisible(false);
					}
//					获取消费分类面板
					CategoryPanel categoryPanel = CategoryPanel.getInstance();
//					将消费分类面板添加到主面板的空面板上
					MainPanel.getInstance().blankPanel.add(categoryPanel.categoryPanel);
//					消费分类面板可视
					CategoryPanel.getInstance().categoryPanel.setVisible(true);

					return;
				}

				CategoryDao categoryDao = new CategoryDao();//消费分类DAO类
				Category category = categoryDao.get(selectedItem);//消费分类Entity类
//				赋值给cid
				cid = category.getId();
//				消费次数更新到数据库
				String consumnNumber = ""+(Integer.parseInt(category.getNumber())+1);
				Category updateCategory = new Category();
				updateCategory.setId(category.getId());
				updateCategory.setName(category.getName());
				updateCategory.setNumber(consumnNumber);
				categoryDao.update(updateCategory);

//				备注
				note = RecordPanel.getInstance().vNote.getText();

//				日期
				if(RecordPanel.getInstance().vDate.getValue() == null) {
					JOptionPane.showMessageDialog(RecordPanel.getInstance().recordPanel, "未选择日期，请从日期面板选择");
					return;
				}

				date = new Date(((java.util.Date)RecordPanel.getInstance().vDate.getValue()).getTime());

//				如果日期格式不正确
				if(!date.toString().matches("^\\d{4}-(0[1-9]|1[0-2])-((0[1-9])|((1|2)[0-9])|30|31)$")) {
//					如2021-01-11
					JOptionPane.showMessageDialog(RecordPanel.getInstance().recordPanel, "日期格式不正确，请从日期面板选择");
					return;
				}

//				添加到数据库
				RecordService.getInstance().addService(spend,cid,note,date);

//				添加成功消息对话框
				JOptionPane.showMessageDialog(RecordPanel.getInstance().recordPanel, "添加成功");

//				更新消费一览面板并且跳转
				this.updateSpendPanel();
//				获取SpendPanel
				SpendPanel spendPanel = SpendPanel.getInstance();
				for(Component c : MainPanel.getInstance().blankPanel.getComponents()) {
//					主面板的空面板上的所有组件消失
					c.setVisible(false);
				}
//				将消费一览面板添加到主面板的空面板上，并设置可视
				MainPanel.getInstance().blankPanel.add(spendPanel.spendPanel);
				SpendPanel.getInstance().spendPanel.setVisible(true);
			}

			public void updateSpendPanel() {

//				获取日历
				Calendar cal = Calendar.getInstance();
				Date currentTime = new Date(System.currentTimeMillis());
//				本年
				int currentYear = cal.get(Calendar.YEAR);
//				本月
				int currentMonth = cal.get(Calendar.MONTH);
//				本日
				int currentDay = cal.get(Calendar.DAY_OF_MONTH);
//				本月多少日
				int allDayOfThisMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

//				记一笔日期
				java.util.Date recordDate = new java.util.Date(date.getTime());
//				更改日期
				cal.setTime(recordDate);
//				获取年份
				int getYear = cal.get(Calendar.YEAR);
//				获取月份
				int getMonth = cal.get(Calendar.MONTH);
//				获取日期
				int getDay = cal.get(Calendar.DAY_OF_MONTH);

//				本月消费
				if(getMonth == currentMonth && getYear == currentYear) {
					SpendService.getInstance().monthSpend += spend;
				}
//				今日消费
				if(getDay == currentDay && getMonth == currentMonth && getYear == currentYear) {
					SpendService.getInstance().daySpend += spend;
				}

	//			日均消费、本月剩余、日均可用与距离月末
	//			更改日期
				cal.setTime(currentTime);
	//			日均消费
				SpendService.getInstance().avgSpendPerDay = SpendService.getInstance().monthSpend/cal.get(Calendar.DAY_OF_MONTH);
	//			本月剩余与日均可用
				Config config = new ConfigDao().getTheLastOne();
				if(config != null) {
					int budgetCount = Integer.parseInt(config.getKey());
					SpendService.getInstance().restofThisMonth = budgetCount-SpendService.getInstance().monthSpend;
					SpendService.getInstance().avgDailyAvailable = SpendService.getInstance().restofThisMonth/(allDayOfThisMonth-currentDay);
				}else {
					SpendService.getInstance().restofThisMonth = -SpendService.getInstance().monthSpend;
					SpendService.getInstance().avgDailyAvailable = 0;
				}

	//			具体月末
				SpendService.getInstance().monthLeftDay = allDayOfThisMonth-currentDay;

//				添加到消费一览面板上
				SpendPanel.getInstance().vMonthSpend.setText("￥"+SpendService.getInstance().monthSpend);
				SpendPanel.getInstance().vDaySpend.setText("￥"+SpendService.getInstance().daySpend);

//				添加到消费一览面板上
				SpendPanel.getInstance().vAvgSpendPerDay.setText("￥"+SpendService.getInstance().avgSpendPerDay);
				SpendPanel.getInstance().vRestofThisMonth.setText("￥"+SpendService.getInstance().restofThisMonth);
				SpendPanel.getInstance().vAvgDailyAvailable.setText("￥"+SpendService.getInstance().avgDailyAvailable);
				SpendPanel.getInstance().vMonthLeftDay.setText(SpendService.getInstance().monthLeftDay+"天");

//				进度条与剩余比例
				int monthSpend = SpendService.getInstance().monthSpend;
				int restofThisMonth = SpendService.getInstance().restofThisMonth;
				SpendPanel.getInstance().progressBar.setBackground(Color.GREEN);
				SpendPanel.getInstance().progressBar.setForeground(Color.BLUE);
				SpendPanel.getInstance().vRestRate.setForeground(Color.GREEN);
				int budget = monthSpend+restofThisMonth;
				if(budget != 0) {
					int spend = (int)Math.round((double)monthSpend/budget*100);
					SpendPanel.getInstance().progressBar.setValue(spend);
					SpendPanel.getInstance().vRestRate.setText("剩余:"+(100-spend)+"%");
				}else {
					SpendPanel.getInstance().progressBar.setValue(100);
					SpendPanel.getInstance().vRestRate.setText("剩余："+0);
				}
			}
		});
	}
}
