/**
 * @File:        CategoryListener.java
 * @Package:     gui.listener
 * @Project:     a_silly_book
 * @Description:
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午5:39:43
 */

package gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dao.CategoryDao;
import gui.model.CategoryComboBoxModel;
import gui.panel.CategoryPanel;
import service.CategoryService;

/**
 * @ClassName:   CategoryListener
 * @Description: 消费分类监听
 * @Author:    	 橙子
 * @Date:        2021年1月1日 下午5:39:43
 */
public class CategoryListener {
//	饿汉式单例模式
	private CategoryListener() {

	}
	private static CategoryListener instance = new CategoryListener();
	public static CategoryListener getInstance() {
		if(null == instance) {
			instance = new CategoryListener();
		}

		return instance;
	}

//	新增、编辑与删除
	public String add = null;
	public String edit = null;
	public String delete = null;

//	方法：添加监听器
	public void addListener() {
//		新增按钮 添加监听
		CategoryPanel.getInstance().bAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				弹出输入对话框
				add = (String)JOptionPane.showInputDialog(CategoryPanel.getInstance().categoryPanel, "", "请输入分类名称", JOptionPane.QUESTION_MESSAGE);//获取用户输入值
//				如果输入不为空
				if(add != null) {
//					获取数据库中的name集合
					ArrayList<String> name = new CategoryDao().getNameList();
//					如果消费分类中不包括用户输入值
					if(!name.contains(add)) {
//						添加到表中
						JTable table = CategoryPanel.getInstance().tCategory;
						Vector<String> line1Data = new Vector<String>();
						line1Data.add(add);
						line1Data.add("0");
						((DefaultTableModel)table.getModel()).addRow(line1Data);
//						添加到数据库
						CategoryService.getInstance().addService(add,"0");
//						添加到“记一笔”中的分类
						CategoryComboBoxModel.getInstance().comboBox.addItem(add);
					}else {
//						如果消费分类中包括用户输入值
						JOptionPane.showMessageDialog(CategoryPanel.getInstance().categoryPanel, "分类重复！请重新输入！");
					}
				}
			}
		});
//		编辑按钮 添加监听
		CategoryPanel.getInstance().bEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				获取选定行的分类名称
				JTable table = CategoryPanel.getInstance().tCategory;
				int row = table.getSelectedRow();//获取选定行
//				如果未选定行，直接返回
				if(row == -1) {
					JOptionPane.showMessageDialog(CategoryPanel.getInstance().categoryPanel, "未选定行");
					return;
				}

				String categoryName = table.getModel().getValueAt(row, 0).toString();

//				弹出输入对话框
				edit = (String)JOptionPane.showInputDialog(CategoryPanel.getInstance().categoryPanel, "原分类名称："+categoryName, "请编辑分类名称", JOptionPane.QUESTION_MESSAGE,null,null,categoryName);//获取用户输入值

//				如果输入不为空
				if(edit == null) {

				}else if(edit.matches("\\s*")){
					JOptionPane.showMessageDialog(CategoryPanel.getInstance().categoryPanel, "分类名称不能为空！");
				}else if(!edit.matches("\\s*")) {
//					获取数据库中的name集合
					ArrayList<String> name = new CategoryDao().getNameList();
//					如果消费分类中不包括用户输入值
					if(!name.contains(edit)) {
//					修改表中数据
						table.getModel().setValueAt(edit,table.getSelectedRow(), 0);
//						修改数据库中数据
						CategoryService.getInstance().editService(edit,categoryName);
//						修改“记一笔”中的分类中的数据
						CategoryComboBoxModel.getInstance().comboBox.removeItem(categoryName);//先移除
						CategoryComboBoxModel.getInstance().comboBox.addItem(edit);//再添加
					}else {
						if(edit.equals(categoryName)) {
//							无动作
						}else {
							JOptionPane.showMessageDialog(CategoryPanel.getInstance().categoryPanel, "分类重复！请重新输入！");
						}
					}
				}
			}
		});
//		删除按钮 添加监听
		CategoryPanel.getInstance().bDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				获取选定行的消费次数
				JTable table = CategoryPanel.getInstance().tCategory;
				int row = table.getSelectedRow();

//				如果未选定行，直接返回
				if(row == -1) {
					JOptionPane.showMessageDialog(CategoryPanel.getInstance().categoryPanel, "未选定行");
					return;
				}

				String consumeNumber = (String)table.getModel().getValueAt(row, 1);

				if(consumeNumber.equals("0") || consumeNumber == null) {
//					如果消费次数为0或空，可以删除
					int option = JOptionPane.showConfirmDialog(CategoryPanel.getInstance().categoryPanel, "确认要删除？");
					if(option == JOptionPane.YES_OPTION) {
						delete = (String)table.getModel().getValueAt(row, 0);
						((DefaultTableModel)table.getModel()).removeRow(row);//删除选定行
//						数据库中也删除
						CategoryService.getInstance().deleteService(delete);
//						删除“记一笔”中的分类
						CategoryComboBoxModel.getInstance().comboBox.removeItem(delete);
					}
				}else {
//					如果消费次数不为0，不能删除
					JOptionPane.showMessageDialog(CategoryPanel.getInstance().categoryPanel,"本分类下有消费记录存在，不能刪除");
				}
			}
		});
	}
}
